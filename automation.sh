# Function to check if the key and cert already exist
function key_and_cert_exist() {
  set +e
  set +x
  aws ssm get-parameter --region eu-west-2 --name "/${ENV}/${APP_NAME}/key" --with-decryption --output text --query "Parameter.Value" > /dev/null
  set -x
  if [[ "$?" == "0" ]]; then
    KEY_IN_SSM="true"
  fi
  #Nullifying command output to stop certificate being logged in to HCS Deployer CW logs
  aws ssm get-parameter --region eu-west-2 --name "/${ENV}/${APP_NAME}/crt" --with-decryption --output text --query "Parameter.Value" > /dev/null
  if [[ "$?" == "0" ]]; then
    CERT_IN_SSM="true"
  fi
  set -e
}

# Error will fail script, and get debugging
set -e
set -x

# Must have environment parameter
if  [ -z ${ENV} ] || [ ${ENV} != "dev" ] && [ ${ENV} != "production" ] && [ ${ENV} != "stage" ] && [ ${ENV} != "test" ]
  then
    echo "Usage: ./apply_${APP_NAME}.bash [environment]"
    echo "Environment must be either dev, test, stage, or production."
    exit 1
fi

# Verify if key pair exist, if not then create a new SSH key pair, and store.
key_pair_exist
if [ "$EXIT_CODE" != "0" ]
  then
    echo "Key pair for ${APP_NAME} not found. Creating a new one & storing it in paramter store."
    set +x
    PRIVATE_KEY=$(aws ec2 create-key-pair --region eu-west-2 --key-name "${APP_NAME}_$ENV" --output text --query "KeyMaterial")
    aws ssm put-parameter --region eu-west-2 --overwrite --name "/$ENV/ssh_key/${APP_NAME}_ssh_key" --type "SecureString" --key-id "$KMS_KEY" --value "$PRIVATE_KEY"
    set -x
    echo "Private key is now in ParameterStore."
fi

# Call the script to to generate key and cert if it doesn't exist
key_and_cert_exist
if [ "$KEY_IN_SSM" != "true" ] || [ "$CERT_IN_SSM" != "true" ] || [ "$NEW_CERTS" == "true" ]
  then
    echo "Key and cert for ${APP_NAME} service not found, or new certs requested. Generating a new key and cert."
    . ${BASE_PATH}/run_scripts/create_cert_and_key.bash ${ENV} ${APP_NAME} "next-cloud.integr-${ENV}.dwpcloud.uk"
fi

# Below command initialises terraform, creates plan and applies it from the configured directory.
cd ${ENV_PATH}
terraform init -reconfigure -backend-config=backend_config/${ENV}.conf
terraform workspace select ${ENV} || terraform workspace new ${ENV}

if [ "$NEW_CERTS" == "true" ]; then
  # Manually taint the certificate to workaround Terraform failing to re-import new certificates
  # For more details see the Terraform code
  terraform taint -module=transfer_your_file aws_acm_certificate.cert
fi

terraform apply -input=false -auto-approve
echo "Deployment for Transfer Your File Succeeded!!!"

