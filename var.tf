variable "cert_workspace" {
  description = "The workspace the ASG should for certs and application config"
}

variable "account_workspace" {
  description = "The workspace name to use for accessing ssm root cert parameters"
}
