data "aws_ssm_parameter" "private_key_pem" {
  name = "/${var.cert_workspace}/transferyourfile/key"
}

data "aws_ssm_parameter" "cert_pem" {
  name = "/${var.cert_workspace}/transferyourfile/crt"
}

data "aws_ssm_parameter" "root_pem" {
  name = "/${var.account_workspace}/root-ca/certificate"
}
