resource "aws_instance" "web" {
  ami           = "ami-0915bcb5fa77e4892"
  instance_type = "t2.micro"
  certificate_arn   = "${aws_acm_certificate.cert.arn}"

  tags = {
    Name = "HelloWorld"
  }
}

resource "aws_acm_certificate" "cert" {
  private_key       = "${data.aws_ssm_parameter.private_key_pem.value}"
  certificate_body  = "${data.aws_ssm_parameter.cert_pem.value}"
  certificate_chain = "${data.aws_ssm_parameter.root_pem.value}"

  # Using create_before_destroy lets you replace a certificate on an existing load balancer
  lifecycle {
    create_before_destroy = true
  }
}
